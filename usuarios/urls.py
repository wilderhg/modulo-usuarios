from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'usuarios.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
  
    url(r'^', include('principal.urls', namespace = "principal")),
     url(r'^', include('registrousuario.urls')),
   
)
