from django.conf.urls import patterns, url
from .views import IndexView

urlpatterns = patterns('',
	url(r'^$', IndexView.as_view()),
	url(r'^salir/$', 'principal.views.LogOut'),

	)