from django.shortcuts import render, redirect
from django.views.generic import FormView
from .forms import UserForm

# Create your views here.
class IndexView(FormView):
	form_class= UserForm
	template_name = 'crearusuario.html'
	success_url = '/'

	def form_valid(self, form):
		user = form.save()
		user.email = form.cleaned_data['email']
		user.save()
		return super(IndexView, self).form_valid(form)
